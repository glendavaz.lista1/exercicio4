import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);  
        Triangulo triangulo = new Triangulo();
        
        System.out.println("Insira o tamanho do lado 1:");
        triangulo.setLado1(le.nextDouble());
        
        System.out.println("Insira o tamanho do lado 2:");
        triangulo.setLado2(le.nextDouble());
        
        System.out.println("Insira o tamanho do lado 3:");
        triangulo.setLado3(le.nextDouble());
        
        System.out.println("O perimetro do triangulo e " + 
        triangulo.calculoPerimetro() + " e ele e do tipo " + triangulo.tipoTriangulo());
    }
  
}
