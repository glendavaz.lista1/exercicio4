public class Triangulo
{
    // instance variables - replace the example below with your own
    private double lado1;
    private double lado2;
    private double lado3;
    
    public Triangulo()
    {
        
    }
    
    public double getLado1() {
        return this.lado1;
    }
    
    public void setLado1(double paramLado1) {
        this.lado1 = paramLado1;
    }
    
    public double getLado2() {
        return this.lado2;
    }
    
    public void setLado2(double paramLado2) {
        this.lado2 = paramLado2;
    }
    
    public double getLado3() {
        return this.lado3;
    }
    
    public void setLado3(double paramLado3) {
        this.lado3 = paramLado3;
    }
    
    public double calculoPerimetro() {
        return this.lado1 + this.lado2 + this.lado3;
    }
    
    public String tipoTriangulo() {
        if(this.lado1 == this.lado2 && this.lado1 == this.lado3) {
            return "Equilatero";
        }else if((this.lado1 == this.lado2 || this.lado1 == this.lado3 || this.lado2 == this.lado3)) {
            return "Isosceles";
        } else {
            return "Escaleno";
        }
    }    
    
}
